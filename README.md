# FileReader

File Reader is an android application which reads the given text file and displays the lines from it randomly.
This app is developed in MVVM pattern and kotlin language

## Screenshots

<img src="https://gitlab.com/nathansdev/filereader/blob/master/screenshots/screen_shot_home_page.png" width="425"/>

<img src="https://gitlab.com/nathansdev/filereader/blob/master/screenshots/screen_shot_splash.png" width="425"/>

<img src="https://gitlab.com/nathansdev/filereader/blob/master/screenshots/screen_shot_toast_heighlight.png" width="425"/>







