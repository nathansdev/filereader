package com.test.android.filereader

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.test.android.filereader.main.FileReaderFlowLayoutActivity
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * UI tests for the implementation of [FileReaderFlowLayoutActivity].
 */
@RunWith(AndroidJUnit4::class)
class FileReaderFlowLayoutActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule(FileReaderFlowLayoutActivity::class.java)

    @Test
    fun testIfViewsAreDisplayed() {
        onView(withId(R.id.progress_main)).check(matches(not(isDisplayed())))
        onView(withId(R.id.button_next)).check(matches(isDisplayed()))
    }

    @Test
    fun testOnNextButtonClicked() {
        onView(withId(R.id.button_next))
            .check(matches(withText(R.string.next)))
            .perform(click())
    }
}