package com.test.android.filereader

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.test.android.filereader.splash.SplashActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * UI tests for the implementation of [SplashActivity].
 */
@RunWith(AndroidJUnit4::class)
class SplashActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule(SplashActivity::class.java)

    @Test
    fun testIsSplashScreenTextIsShown() {
        onView(withText(R.string.app_name)).check(matches(isDisplayed()))
    }
}