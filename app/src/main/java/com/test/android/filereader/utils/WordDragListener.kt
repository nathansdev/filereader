package com.test.android.filereader.utils

import android.view.DragEvent
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.test.android.filereader.view.FlowLayout
import timber.log.Timber

/**
 * Drag and drop event listener implements [View.OnDragListener]
 */
class WordDragListener(private val eventHandler: DragEventHandler) : View.OnDragListener {
    override fun onDrag(view: View?, event: DragEvent?): Boolean {
        when (event?.action) {
            DragEvent.ACTION_DRAG_ENTERED -> {
                when (view) {
                    is FlowLayout -> {
                        Timber.d("onDrag Entered Flow Layout")
                    }
                    is TextView -> {
                        Timber.d("onDrag Entered TextView %s", view.text)
                        eventHandler.onEntered(view)
                    }
                }
            }
            DragEvent.ACTION_DRAG_EXITED -> {
                when (view) {
                    is FlowLayout -> {
                        Timber.d("onDrag Exited Flow Layout")
                    }
                    is TextView -> {
                        Timber.d("onDrag Exited TextView %s", view.text)
                        eventHandler.onExited(view)
                    }
                }
            }
            DragEvent.ACTION_DROP -> {
                when (view) {
                    is RecyclerView -> {
                        Timber.d("onDrag Drop RecyclerView %s", event.localState)
                        eventHandler.onDropped(view, event)
                    }
                    is FlowLayout -> {
                        Timber.d("onDrag Drop Flow Layout state %s %s", view, event.localState)
                        eventHandler.onDropped(view, event)
                    }
                    is TextView -> {
                        Timber.d("onDrag Drop TextView and state %s %s", view, event.localState)
                        eventHandler.onDropped(view, event)
                    }
                }
            }
            DragEvent.ACTION_DRAG_LOCATION -> {
                when (view) {
                    is RecyclerView -> {
                        Timber.d("onDrag Location Recycler view")
                        eventHandler.onLocation(view, event)
                    }
                    is FlowLayout -> {
                        Timber.d("onDrag Location Flow Layout")
                    }
                    is TextView -> {
                        Timber.d("onDrag Location TextView %s", view.text)
                        eventHandler.onLocation(view, event)
                    }
                }
            }
            DragEvent.ACTION_DRAG_ENDED -> {
                when (view) {
                    is RecyclerView -> {
                        Timber.d("onDrag Ended RecyclerView %s", event.localState)
                        eventHandler.onEnded(view, event)
                    }
                    is FlowLayout -> {
                        Timber.d("onDrag Ended Flow Layout %s", event.localState)
                        eventHandler.onEnded(view, event)
                    }
                    is TextView -> {
                        Timber.d("onDrag Ended TextView %s", view.text)
                        eventHandler.onEnded(view)
                    }
                }
            }
        }
        return true
    }
}