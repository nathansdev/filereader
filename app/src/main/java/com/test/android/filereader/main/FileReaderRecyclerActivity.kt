package com.test.android.filereader.main

import android.content.ClipData
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.DragEvent
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.test.android.filereader.Constants
import com.test.android.filereader.R
import com.test.android.filereader.base.BaseActivity
import com.test.android.filereader.extension.*
import com.test.android.filereader.main.adapter.WordsAdapter
import com.test.android.filereader.model.ErrorState
import com.test.android.filereader.model.ViewState
import com.test.android.filereader.model.Word
import com.test.android.filereader.model.WordItemViewData
import com.test.android.filereader.utils.*
import com.test.android.filereader.view.FlowLayout
import kotlinx.android.synthetic.main.activity_words_recycler_view.*
import timber.log.Timber
import java.util.regex.Pattern

/**
 * The main screen of application uses [recycler_view] to show list of words
 */
class FileReaderRecyclerActivity : BaseActivity(), View.OnClickListener,
    View.OnLongClickListener, DragEventHandler, WordClickListener {

    private lateinit var viewModel: FileReaderViewModel
    private var listOfWords: ArrayList<Word> = arrayListOf()
    private var listOfWordItemViewData: ArrayList<WordItemViewData> = arrayListOf()
    private var listOfWordItemViewDataRandomized: ArrayList<WordItemViewData> = arrayListOf()
    private var indexesOfPunctuation = arrayListOf<Int>()
    private var listOfPunctuations = arrayListOf<WordItemViewData>()
    private lateinit var adapter: WordsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_words_recycler_view)
        setUpViews()
        setUpViewModel()
    }

    /**
     * initialize views
     */
    private fun setUpViews() {
        next.setOnClickListener(this)
        val layoutManager = FlexboxLayoutManager(this)
        adapter = WordsAdapter(listOfWordItemViewDataRandomized, this, this)
        recycler_view.setOnDragListener(WordDragListener(this))
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.FLEX_START
        recycler_view.layoutManager = layoutManager
        recycler_view.adapter = adapter
    }

    /**
     * initialize view model and observes for state changes
     */
    private fun setUpViewModel() {
        viewModel = this.let {
            ViewModelProvider(this)[FileReaderViewModel::class.java]
        }
        viewModel.viewState.observe(this, Observer {
            it?.let { renderUI(it) }
        })
        viewModel.errorState.observe(this, Observer {
            it?.let { renderError(it) }
        })
        viewModel.loadFileFromAsset()
    }

    /**
     * update the ui when state changes
     */
    private fun renderUI(state: ViewState) {
        when (state.isLoading) {
            true -> {
                progress.show()
                next.gone()
            }
            false -> {
                progress.gone()
                next.show()
            }
        }
        setUpWords(state.randomLine)
    }

    /**
     * renders error for this activity
     */
    private fun renderError(state: ErrorState) {
        when (state.showError) {
            true -> {
                showToast(state.message)
                next.gone()
            }
        }
    }

    /**
     * adds words from randomLine to [recycler_view]
     */
    private fun setUpWords(text: String) {
        if (text.isEmpty()) {
            return
        }
        if (listOfWords.isNotEmpty()) {
            listOfWords.clear()
        }
        if (listOfWordItemViewData.isNotEmpty()) {
            listOfWordItemViewData.clear()
        }
        if (indexesOfPunctuation.isNotEmpty()) {
            indexesOfPunctuation.clear()
        }
        if (listOfPunctuations.isNotEmpty()) {
            listOfPunctuations.clear()
        }
        if (listOfWordItemViewDataRandomized.isNotEmpty()) {
            listOfWordItemViewDataRandomized.clear()
        }
        val m =
            Pattern.compile(Constants.REGEX_WORDS_SEPARATION_BY_SPACE_AND_PUNCTUATION).matcher(text)
        while (m.find()) {
            val word = Word(
                value = m.group(),
                typeWordOrPunctuation = if (m.group().hasPunctuations()) Constants.TYPE_PUNCTUATION else Constants.TYPE_WORD
            )
            listOfWords.add(word)
        }
        listOfWords.forEachIndexed { index, it ->
            val wordItemViewData =
                WordItemViewData(
                    word = it,
                    prefix = listOfWords.getBeforeOrNull(index - 1),
                    suffix = listOfWords.getNextOrNull(index + 1),
                    position = index
                )
            if (wordItemViewData.isPunctuation) {
                indexesOfPunctuation.add(index)
                listOfPunctuations.add(wordItemViewData)
            }
            listOfWordItemViewData.add(wordItemViewData)
        }
        Timber.d(
            "list of words %s %s",
            listOfWords,
            listOfWords.size
        )
        Timber.d(
            "list of words view data separated by regex %s %s",
            listOfWordItemViewData,
            listOfWordItemViewData.size
        )
        Timber.d(
            "indexes of punctuation %s %s",
            indexesOfPunctuation,
            indexesOfPunctuation.size
        )
        Timber.d(
            "list of punctuations %s %s",
            listOfPunctuations,
            listOfPunctuations.size
        )

        //sorting in descending order to remove these indexes from [listOfWordItemViewData]
        indexesOfPunctuation.sortDescending()
        Timber.d("indexes after sorting %s", indexesOfPunctuation)

        indexesOfPunctuation.forEach {
            listOfWordItemViewData.removeAt(it)
        }
        Timber.d("words data after removing the indexes %s", listOfWordItemViewData)

        listOfWordItemViewData.shuffle()
        Timber.d("words data after shuffle %s", listOfWordItemViewData)

        //sorting in ascending order to add these indexes back to original positions in [listOfWordItemViewData]
        indexesOfPunctuation.sort()
        Timber.d("indexes after sorting %s", indexesOfPunctuation)

        listOfPunctuations.forEach {
            val itemViewData = it
            if (itemViewData.position!! < listOfWordItemViewData.size) {
                listOfWordItemViewData.add(itemViewData.position, itemViewData)
            } else {
                listOfWordItemViewData.add(itemViewData)
            }
        }

        //adding shuffled words data to adapter list
        listOfWordItemViewData.forEachIndexed { index, wordItemViewData ->
            listOfWordItemViewDataRandomized.add(index, wordItemViewData.copy(position = index))
        }
        adapter.notifyDataSetChanged()
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.next -> {
                viewModel.getRandomLineFromList()
            }
        }
    }

    override fun onLongClick(view: View?): Boolean {
        return true
    }

    override fun onWordItemClicked(data: WordItemViewData) {
        showToast(Constants.TOAST_PREFIX_TEXT + data.word?.value)
    }

    override fun onEnded(view: TextView) {
        val viewData = view.tag as WordItemViewData
        updateItemToNormalAt(arrayListOf(viewData))
        updateItemToNormalAt(listOfWordItemViewDataRandomized.getHighLightedItems())
    }

    override fun onExited(view: TextView) {
        updateItemToNormalAt(listOfWordItemViewDataRandomized.getHighLightedItems())
    }

    override fun onEntered(view: TextView) {

    }

    /**
     * handles the on drag call on views when the another view hover on them
     */
    override fun onLocation(view: TextView, event: DragEvent) {
        val viewData = view.tag as WordItemViewData
        val indexOfThisView = viewData.position!!
        Timber.d("onLocation text view data %s", indexOfThisView)
        when (viewData.word?.typeWordOrPunctuation) {
            Constants.TYPE_PUNCTUATION -> {
                when (indexOfThisView) {
                    0 -> {
                        highLightItemAt(
                            listOfWordItemViewDataRandomized.getNextAdapterWordData(
                                indexOfThisView
                            )
                        )
                    }
                    listOfWordItemViewDataRandomized.size - 1 -> {
                        highLightItemAt(
                            listOfWordItemViewDataRandomized.getPreviousAdapterWordData(
                                indexOfThisView
                            )
                        )
                    }
                    else -> {
                        handleOnDragOverPunctuations(
                            indexOfThisView,
                            Utils.isDragPointInsideLeftOrRightHalfOfView(view, event)
                        )
                    }
                }
            }
            Constants.TYPE_WORD -> {
                when (indexOfThisView) {
                    0 -> {
                        highLightItemAt(indexOfThisView)
                    }
                    listOfWordItemViewDataRandomized.size - 1 -> {
                        highLightItemAt(indexOfThisView)
                    }
                    else -> {
                        handleOnDragOverWords(
                            indexOfThisView,
                            Utils.isDragPointInsideLeftOrRightHalfOfView(view, event)
                        )
                    }
                }
            }
        }
    }

    override fun onLocation(view: FlowLayout, event: DragEvent) {

    }

    override fun onDragStart(text: TextView) {
        var viewData = text.tag as WordItemViewData
        viewData = viewData.copy(show = false)
        Timber.d("onDragStart %s", viewData)
        updateItem(viewData)
        val clipData = ClipData.newPlainText("value", text.text)
        text.startDrag(
            clipData,
            CustomDragShadow(
                text,
                ColorDrawable(R.drawable.bg_word_high_light)
            ),
            viewData, 0
        )
    }

    override fun onDropped(view: TextView, event: DragEvent) {
        val draggedData = (event.localState as WordItemViewData)
        val viewData = view.tag as WordItemViewData
        val indexOfThisView = viewData.position!!
        val indexOfDroppedView = draggedData.position!!
        Timber.d(
            "onDropped on text view indexOfThisView indexOfDroppedView %s %s",
            indexOfThisView,
            indexOfDroppedView
        )
        when (viewData.word?.typeWordOrPunctuation) {
            Constants.TYPE_PUNCTUATION -> {
                when (indexOfThisView) {
                    0 -> {
                        addItemAt(draggedData, indexOfThisView + 1)
                    }
                    listOfWordItemViewDataRandomized.size - 1 -> {
                        addItemAt(draggedData, indexOfThisView - 1)
                    }
                    else -> {
                        handleOnDropOverPunctuations(
                            indexOfThisView,
                            Utils.isDragPointInsideLeftOrRightHalfOfView(view, event),
                            draggedData
                        )
                    }
                }
            }
            Constants.TYPE_WORD -> {
                when (indexOfThisView) {
                    0 -> {
                        addItemAt(draggedData, indexOfThisView)
                    }
                    listOfWordItemViewDataRandomized.size - 1 -> {
                        addItemAt(draggedData, indexOfThisView)
                    }
                    else -> {
                        handleOnDropOverWords(
                            indexOfThisView,
                            Utils.isDragPointInsideLeftOrRightHalfOfView(view, event),
                            draggedData
                        )
                    }
                }
            }
        }
    }

    override fun onDropped(view: FlowLayout, event: DragEvent) {

    }

    override fun onDropped(view: RecyclerView, event: DragEvent) {
        var draggedData = (event.localState as WordItemViewData)
        val indexOfDroppedView = draggedData.position!!
        val pairLocationData = Utils.getViewDataAndDragPositionUnderTouchPoint(view, event)
        val viewData = pairLocationData?.second
        val indexOfThisView = viewData?.position
        if (indexOfThisView == null) {
            draggedData = draggedData.copy(show = true)
            Timber.d("onDropped on recycler view false %s", draggedData)
            updateItem(draggedData)
            return
        }
        Timber.d(
            "onDropped on recycler view true indexOfThisView indexOfDroppedView %s %s",
            indexOfThisView,
            indexOfDroppedView
        )
        when (viewData.word?.typeWordOrPunctuation) {
            Constants.TYPE_PUNCTUATION -> {
                when (indexOfThisView) {
                    0 -> {
                        addItemAt(draggedData, indexOfThisView + 1)
                    }
                    listOfWordItemViewDataRandomized.size - 1 -> {
                        addItemAt(draggedData, indexOfThisView - 1)
                    }
                    else -> {
                        handleOnDropOverPunctuations(
                            indexOfThisView,
                            pairLocationData.first,
                            draggedData
                        )
                    }
                }
            }
            Constants.TYPE_WORD -> {
                when (indexOfThisView) {
                    0 -> {
                        addItemAt(draggedData, indexOfThisView)
                    }
                    listOfWordItemViewDataRandomized.size - 1 -> {
                        addItemAt(draggedData, indexOfThisView)
                    }
                    else -> {
                        handleOnDropOverWords(indexOfThisView, pairLocationData.first, draggedData)
                    }
                }
            }
        }
    }

    override fun onLocation(view: RecyclerView, event: DragEvent) {
        val pairLocationData = Utils.getViewDataAndDragPositionUnderTouchPoint(view, event)
        val viewData = pairLocationData?.second
        val indexOfThisView = viewData?.position
        if (indexOfThisView == null) {
            updateItemToNormalAt(listOfWordItemViewDataRandomized.getHighLightedItems())
            return
        }
        Timber.d("onLocation recycler view data %s %s", pairLocationData, indexOfThisView)
        when (viewData.word?.typeWordOrPunctuation) {
            Constants.TYPE_PUNCTUATION -> {
                when (indexOfThisView) {
                    0 -> {
                        highLightItemAt(
                            listOfWordItemViewDataRandomized.getNextAdapterWordData(
                                indexOfThisView
                            )
                        )
                    }
                    listOfWordItemViewDataRandomized.size - 1 -> {
                        highLightItemAt(
                            listOfWordItemViewDataRandomized.getPreviousAdapterWordData(
                                indexOfThisView
                            )
                        )
                    }
                    else -> {
                        handleOnDragOverPunctuations(indexOfThisView, pairLocationData.first)
                    }
                }
            }
            Constants.TYPE_WORD -> {
                when (indexOfThisView) {
                    0 -> {
                        highLightItemAt(indexOfThisView)
                    }
                    listOfWordItemViewDataRandomized.size - 1 -> {
                        highLightItemAt(indexOfThisView)
                    }
                    else -> {
                        handleOnDragOverWords(indexOfThisView, pairLocationData.first)
                    }
                }
            }
        }
    }

    override fun onEnded(view: RecyclerView, event: DragEvent) {
        var viewData = (event.localState as WordItemViewData)
        viewData = viewData.copy(show = true)
        Timber.d("onEnded %s", viewData)
    }

    override fun onEnded(view: FlowLayout, event: DragEvent) {

    }

    /**
     * handles drop event fail and updates dragged back to its position
     */
    private fun handleOnDropFailed(draggedData: WordItemViewData) {
        updateItem(draggedData.copy(show = true))
    }

    /**
     * handles on drop event on punctuations and insert new element
     */
    private fun handleOnDropOverPunctuations(
        indexOfThisView: Int,
        dragPosition: Int,
        draggedData: WordItemViewData
    ) {
        when (dragPosition) {
            Constants.INDEX_LEFT -> {
                addItemAt(draggedData, indexOfThisView)
            }
            Constants.INDEX_RIGHT -> {
                addItemAt(draggedData, indexOfThisView + 1)
            }
        }
    }

    /**
     * handles on drop event on words and inserts new element
     */
    private fun handleOnDropOverWords(
        indexOfThisView: Int,
        dragPosition: Int,
        draggedData: WordItemViewData
    ) {
        when (dragPosition) {
            Constants.INDEX_LEFT -> {
                var previousData =
                    listOfWordItemViewDataRandomized.getPreviousAdapterWordData(indexOfThisView - 1)
                //previous data will be null for first word with punctuations
                if (previousData == null) {
                    previousData =
                        listOfWordItemViewData.getPreviousAdapterWordData(indexOfThisView)
                }
                if (previousData?.show != null && previousData.show) {
                    addItemAt(draggedData, indexOfThisView)
                } else {
                    handleOnDropFailed(draggedData)
                }
            }
            Constants.INDEX_RIGHT -> {
                var nextData =
                    listOfWordItemViewDataRandomized.getNextAdapterWordData(indexOfThisView + 1)
                //next data will be null for last word with punctuations
                if (nextData == null) {
                    nextData = listOfWordItemViewData.getNextAdapterWordData(indexOfThisView)
                }
                if (nextData?.show != null && nextData.show) {
                    addItemAt(draggedData, indexOfThisView + 1)
                } else {
                    handleOnDropFailed(draggedData)
                }
            }
        }
    }

    /**
     * handles on drag over the punctuation words and highlights next and previous views(words)
     * if they are visible]
     */
    private fun handleOnDragOverPunctuations(indexOfThisView: Int, dragPosition: Int) {
        when (dragPosition) {
            Constants.INDEX_LEFT -> {
                highLightItemAt(
                    listOfWordItemViewDataRandomized.getPreviousAndNextWordData(
                        indexOfThisView
                    )
                )
            }
            Constants.INDEX_RIGHT -> {
                highLightItemAt(
                    listOfWordItemViewDataRandomized.getPreviousAndNextWordData(
                        indexOfThisView
                    )
                )
            }
        }
    }

    /**
     * handles on drag over the words and highlights next and previous views if they are visible]
     */
    private fun handleOnDragOverWords(indexOfThisView: Int, dragPosition: Int) {
        when (dragPosition) {
            Constants.INDEX_LEFT -> {
                highLightItemAt(
                    listOfWordItemViewDataRandomized.getThisAndPreviousAdapterWordData(
                        indexOfThisView
                    )
                )
            }
            Constants.INDEX_RIGHT -> {
                highLightItemAt(
                    listOfWordItemViewDataRandomized.getThisAndNextAdapterWordData(
                        indexOfThisView
                    )
                )
            }
        }
    }

    /**
     * adds [shits dragged element to dropped position]
     */
    private fun addItemAt(itemViewData: WordItemViewData, newIndex: Int) {
        val updateList = Utils.getReOrderedListOfWords(
            listOfWordItemViewDataRandomized, indexesOfPunctuation, newIndex, itemViewData
        )
        updateItems(updateList)
    }

    /**
     *updates the background to highlight mode on hover mode
     */
    private fun highLightItemAt(vararg indexes: Int) {
        updateItemToNormalAt(listOfWordItemViewDataRandomized.getHighLightedItems())
        try {
            indexes.forEach {
                Timber.d("highLightItemAt %s", it)
                val data =
                    listOfWordItemViewDataRandomized[it].copy(highLightStatus = Constants.HIGHLIGHTED)
                listOfWordItemViewDataRandomized[it] = data
            }
            adapter.notify()
        } catch (e: IndexOutOfBoundsException) {
            Timber.e(e)
        }
    }

    /**
     *updates the background to highlight mode on hover mode
     */
    private fun highLightItemAt(itemViewData: WordItemViewData?) {
        updateItemToNormalAt(listOfWordItemViewDataRandomized.getHighLightedItems())
        try {
            itemViewData?.let { it ->
                it.position.let {
                    val data =
                        listOfWordItemViewDataRandomized[it!!].copy(highLightStatus = Constants.HIGHLIGHTED)
                    listOfWordItemViewDataRandomized[it] = data
                    adapter.notify()
                }
            }
        } catch (e: IndexOutOfBoundsException) {
            Timber.e(e)
        }
    }

    /**
     *updates the background to highlight mode on hover mode
     */
    private fun highLightItemAt(list: ArrayList<WordItemViewData>) {
        updateItemToNormalAt(listOfWordItemViewDataRandomized.getHighLightedItems())
        try {
            list.forEach {
                val index = listOfWordItemViewDataRandomized.indexOf(it)
                Timber.d("highLightItemAt %s", it)
                val data =
                    listOfWordItemViewDataRandomized[it.position!!].copy(highLightStatus = Constants.HIGHLIGHTED)
                listOfWordItemViewDataRandomized[it.position] = data
            }
            adapter.notify()
        } catch (e: IndexOutOfBoundsException) {
            Timber.e(e)
        }
    }

    /**
     *update particular item with new data
     */
    private fun updateItem(data: WordItemViewData) {
        try {
            listOfWordItemViewDataRandomized[data.position!!] = data
            adapter.notify()
        } catch (e: IndexOutOfBoundsException) {
            Timber.e(e)
        }
    }

    /**
     *updates the entire list with reordered list on drop
     */
    private fun updateItems(updateList: ArrayList<WordItemViewData>) {
        Timber.d("updatedList %s", updateList)
        listOfWordItemViewDataRandomized.clear()
        listOfWordItemViewDataRandomized.addAll(updateList)
        adapter.notify()
    }

    /**
     *updates the unfocused items background to normal
     */
    private fun updateItemToNormalAt(vararg indexes: Int) {
        try {
            indexes.forEach {
                Timber.d("updateItemToNormalAt %s", it)
                val data =
                    listOfWordItemViewDataRandomized[it].copy(highLightStatus = Constants.NORMAL)
                listOfWordItemViewDataRandomized[it] = data
            }
            adapter.notify()
        } catch (e: IndexOutOfBoundsException) {
            Timber.e(e)
        }
    }

    /**
     *updates the unfocused items background to normal
     */
    private fun updateItemToNormalAt(list: ArrayList<WordItemViewData>) {
        try {
            list.forEach {
                Timber.d("updateItemToNormalAt %s", it.position)
                val data =
                    listOfWordItemViewDataRandomized[it.position!!].copy(highLightStatus = Constants.NORMAL)
                listOfWordItemViewDataRandomized[it.position] = data
            }
            adapter.notify()
        } catch (e: IndexOutOfBoundsException) {
            Timber.e(e)
        }
    }
}
