package com.test.android.filereader.base

import android.os.Bundle
import android.view.Gravity
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


/**
 * Base activity class that abstracts common functions between all activities.
 */
abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    /**
     * common toast method.
     */
    protected fun showToast(message: String) {
        val toast = Toast.makeText(
            applicationContext,
            message, Toast.LENGTH_SHORT
        )
        toast.setGravity(Gravity.TOP, 0, 0)
        toast.show()
    }
}
