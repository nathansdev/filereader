package com.test.android.filereader.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.test.android.filereader.Constants
import com.test.android.filereader.R
import com.test.android.filereader.extension.getNextAdapterWordData
import com.test.android.filereader.extension.getPreviousAdapterWordData
import com.test.android.filereader.model.WordItemViewData
import com.test.android.filereader.utils.DragEventHandler
import com.test.android.filereader.utils.WordClickListener
import com.test.android.filereader.utils.WordDragListener

/**
 * Recyclerview adapter for displaying words in list
 */
class WordsAdapter(
    private val listOfItems: ArrayList<WordItemViewData>,
    private val dragEventHandler: DragEventHandler,
    private val wordClickListener: WordClickListener
) :
    RecyclerView.Adapter<WordsAdapter.WordItemViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WordItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_word_item, parent, false)
        return WordItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: WordItemViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemCount(): Int {
        return listOfItems.size
    }

    inner class WordItemViewHolder constructor(itemView: View) : RecyclerView.ViewHolder
        (itemView) {
        private val text: TextView = itemView.findViewById(R.id.text_item)

        fun onBind(position: Int) {
            val data = listOfItems[position]
            //shows and hides based on drag and drop event
            text.visibility = if (data.show) View.VISIBLE else View.INVISIBLE
            text.text = data.word?.value
            text.setBackgroundResource(getBackgroundResource(position))
            text.tag = data.copy(position = position)
            text.setOnDragListener(WordDragListener(eventHandler = dragEventHandler))
            if (data.word?.typeWordOrPunctuation == Constants.TYPE_WORD) {
                text.setOnLongClickListener {
                    dragEventHandler.onDragStart(text)
                    true
                }
                text.setOnClickListener { wordClickListener.onWordItemClicked(data) }
            }
        }
    }

    /**
     * returns the background based on prefix and suffix check
     */
    private fun getBackgroundResource(position: Int): Int {
        val data = listOfItems[position]
        if (data.highLightStatus == Constants.HIGHLIGHTED) {
            return R.drawable.bg_word_high_light
        } else {
            when (data.word?.typeWordOrPunctuation) {
                Constants.TYPE_PUNCTUATION -> {
                    return R.drawable.bg_word_grey
                }
                Constants.TYPE_WORD -> {
                    when (position) {
                        0 -> {
                            val nextViewData = listOfItems.getNextAdapterWordData(position + 1)
                            return if (data.suffix.equals(nextViewData?.word?.value)) {
                                R.drawable.bg_word_green
                            } else {
                                R.drawable.bg_word_red
                            }
                        }
                        listOfItems.size - 1 -> {
                            val previousViewData =
                                listOfItems.getPreviousAdapterWordData(position - 1)
                            return if (data.prefix.equals(previousViewData?.word?.value)) {
                                R.drawable.bg_word_green
                            } else {
                                R.drawable.bg_word_red
                            }
                        }
                        else -> {
                            val previousViewData =
                                listOfItems.getPreviousAdapterWordData(position - 1)
                            val nextViewData = listOfItems.getNextAdapterWordData(position + 1)
                            return if (data.prefix.equals(previousViewData?.word?.value)
                                && data.suffix.equals(nextViewData?.word?.value)
                            ) {
                                R.drawable.bg_word_green
                            } else if (!data.prefix.equals(previousViewData?.word?.value)
                                && !data.suffix.equals(nextViewData?.word?.value)
                            ) {
                                R.drawable.bg_word_red
                            } else {
                                R.drawable.bg_word_yellow
                            }
                        }
                    }
                }
                else -> {
                    return R.drawable.bg_word_grey
                }
            }
        }
    }
}