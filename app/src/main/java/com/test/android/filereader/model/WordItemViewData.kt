package com.test.android.filereader.model

import com.test.android.filereader.Constants

/*
* ui state class for single word item view
 */
data class WordItemViewData(
    val word: Word? = null,
    val prefix: String? = null,
    val suffix: String? = null,
    val position: Int? = null,
    val highLightStatus: String = Constants.NORMAL,
    val isWord: Boolean = word?.typeWordOrPunctuation == Constants.TYPE_WORD,
    val isPunctuation: Boolean = word?.typeWordOrPunctuation == Constants.TYPE_PUNCTUATION,
    val show: Boolean = true
)