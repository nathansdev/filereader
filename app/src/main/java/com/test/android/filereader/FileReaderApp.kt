package com.test.android.filereader

import android.app.Application
import android.content.Context
import com.test.android.filereader.log.NotLoggingTree
import timber.log.Timber

/**
 * Application class instantiates Logging library
 */
class FileReaderApp : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(NotLoggingTree())
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }
}