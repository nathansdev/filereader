package com.test.android.filereader.log


import timber.log.Timber

/**
 * Timber base logtree for release build.
 */
class NotLoggingTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
    }
}
