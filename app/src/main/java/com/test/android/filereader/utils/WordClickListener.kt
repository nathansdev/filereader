package com.test.android.filereader.utils

import com.test.android.filereader.model.WordItemViewData

interface WordClickListener {
    fun onWordItemClicked(data: WordItemViewData)
}