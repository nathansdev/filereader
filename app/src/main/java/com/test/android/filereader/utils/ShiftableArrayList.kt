import java.util.*

/**
 * arraylist shifts elements from one index to another with immovable fixed indexs
 */
class ShiftableArrayList<T> : ArrayList<T>() {
    // ascending and descending views of the fixed indices
    private val fixedAsc: SortedSet<Int>
    private val fixedDec: SortedSet<Int>

    init {
        val treeSet = TreeSet<Int>()
        fixedAsc = treeSet
        fixedDec = treeSet.descendingSet()
    }

    fun setFixedPositions(indexes: ArrayList<Int>): Boolean {
        return fixedAsc.addAll(indexes)
    }

    fun shift(fromInd: Int, toInd: Int) {
        if (fromInd == toInd) {
            return
        }

        super.add(toInd, super.removeAt(fromInd))

        if (toInd < fromInd) {
            // all between `from` and `to` shifted up, swap fixed indices down back into position
            // iterate from low (toInd) to high (fromInd)
            for (i in fixedAsc.subSet(toInd, fromInd)) {
                super.add(i, super.removeAt(i + 1))
            }
        } else {
            // all between `from` and `to` shifted down, swap fixed indices up back into position
            // iterate from high (toInd) to low (fromInd)
            for (i in fixedDec.subSet(toInd, fromInd)) {
                super.add(i, super.removeAt(i - 1))
            }
        }
    }
}
