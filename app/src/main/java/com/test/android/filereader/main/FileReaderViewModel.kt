package com.test.android.filereader.main

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.test.android.filereader.Constants
import com.test.android.filereader.model.ErrorState
import com.test.android.filereader.model.ViewState
import timber.log.Timber
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import kotlin.random.Random

/**
 * ViewModel for the FileReader Activity.
 */
class FileReaderViewModel(application: Application) : AndroidViewModel(application) {
    private var lines: ArrayList<String> = ArrayList()
    var loadTask: LoadFileFromAssetTask? = null
    private val _viewState = MutableLiveData<ViewState>()
    private val _errorState = MutableLiveData<ErrorState>()
    val viewState: LiveData<ViewState> = _viewState
    val errorState: LiveData<ErrorState> = _errorState

    private var lastRandomNumber: Int? = null

    init {
        _viewState.value = ViewState()
        _errorState.value = ErrorState()
        loadTask = LoadFileFromAssetTask(application)
    }

    //triggers async task to load text file
    fun loadFileFromAsset() {
        loadTask!!.execute()
    }

    //return current ui data class
    fun currentViewState(): ViewState = viewState.value!!

    //return current error data class
    fun currentErrorState(): ErrorState = errorState.value!!

    fun loadingStarted() {
        _viewState.value = currentViewState().copy(isLoading = true)
    }

    fun loadingFinished() {
        _viewState.value = currentViewState().copy(isLoading = false)
    }

    fun randomLineChanged(newLine: String) {
        _viewState.value = currentViewState().copy(randomLine = newLine)
    }

    fun onError(errorText: String) {
        _errorState.value = currentErrorState().copy(showError = true, message = errorText)
    }

    /**
     *loads and reads all the line from the asset file
     */
    open inner class LoadFileFromAssetTask(private val application: Application) :
        AsyncTask<Void, Void, ArrayList<String>>() {

        override fun onPreExecute() {
            loadingStarted()
        }

        override fun doInBackground(vararg p0: Void?): ArrayList<String> {
            //delay for smooth ui
            Thread.sleep(2000)
            val lines: ArrayList<String> = ArrayList()
            var bufferedReader: BufferedReader? = null
            try {
                val inputStream: InputStream? = application.assets?.open(Constants.FILE_NAME)
                bufferedReader = BufferedReader(InputStreamReader(inputStream))
                var line = bufferedReader.readLine()
                while (line != null) {
                    line = bufferedReader.readLine()
                    if (line != null && line.isNotEmpty()) {
                        lines.add(line)
                    }
                }
            } catch (e: IOException) {
                Timber.e(e.localizedMessage)
            } finally {
                bufferedReader?.close()
            }
            return lines
        }

        override fun onPostExecute(result: ArrayList<String>?) {
            loadingFinished()
            if (result != null) {
                lines.addAll(result)
                if (lines.isEmpty()) {
                    onError("Couldn't load the text file, Please try again later")
                } else {
                    getRandomLineFromList()
                }
            }
        }
    }

    /**
     *finds random line from list of lines from assset
     */
    fun getRandomLineFromList() {
        val number = getRandomNumber()
        try {
            randomLineChanged(lines[number])
        } catch (e: Exception) {
            Timber.e(e.localizedMessage)
            randomLineChanged(lines[number])
        }
    }

    /**
     * generates new random number every time given the limit
     */
    private fun getRandomNumber(): Int {
        var randomNumber = Random.nextInt(lines.size)
        if (randomNumber == lastRandomNumber) {
            randomNumber = getRandomNumber()
        }
        lastRandomNumber = randomNumber
        return randomNumber
    }
}