package com.test.android.filereader.model

import com.test.android.filereader.Constants

data class Word(
    val value: String = "",
    val typeWordOrPunctuation: Int = Constants.TYPE_PUNCTUATION
)