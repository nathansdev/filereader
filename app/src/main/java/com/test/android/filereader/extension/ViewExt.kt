package com.test.android.filereader.extension

import android.graphics.Point
import android.view.View
import android.view.ViewGroup
import androidx.core.view.forEach
import androidx.core.view.forEachIndexed
import androidx.core.view.get
import androidx.core.view.size
import com.test.android.filereader.Constants
import com.test.android.filereader.model.WordItemViewData
import kotlinx.android.synthetic.main.activity_words_flow_layout.*

/**
 * Extension functions related to view
 */

fun View.show() {
    this.visibility = View.VISIBLE
}


fun View.hide() {
    this.visibility = View.INVISIBLE
}


fun View.gone() {
    this.visibility = View.GONE
}


/**
 * returns the x and y location of view in screen (left top corner x,y)
 */
fun View.getLocationOnScreen(): Point {
    val location = IntArray(2)
    this.getLocationOnScreen(location)
    return Point(location[0], location[1])
}


/**
 * returns the current, suffix, prefix view for onExit drag event
 */
fun View.getPreviousAndNextView(viewGroup: ViewGroup): List<View> {
    val items = arrayListOf<View>()
    val index = viewGroup.indexOfChild(this)
    items.add(this)
    if (index + 1 < viewGroup.size) {
        items.add(viewGroup[index + 1])
    }
    if (index - 1 >= 0) {
        items.add(viewGroup[index - 1])
    }
    return items
}


/**
 * returns the current view and preview view for left side drag
 * prefix view should be visible
 */
fun View.getCurrentAndPreviousView(viewGroup: ViewGroup): List<View> {
    val items = arrayListOf<View>()
    val index = viewGroup.indexOfChild(this)
    items.add(this)
    val previousView = viewGroup.getPreviousViewOrNull(index - 1)
    if (previousView != null) {
        items.add(previousView)
    }
    return items
}


/**
 * returns the current view and suffix view for right side drag
 * suffix view should be visible
 */
fun View.getCurrentAndNextView(viewGroup: ViewGroup): List<View> {
    val items = arrayListOf<View>()
    val index = viewGroup.indexOfChild(this)
    items.add(this)
    val nextView = viewGroup.getNextViewOrNull(index + 1)
    if (nextView != null) {
        items.add(nextView)
    }
    return items
}


fun View.isOnTheLeftEdgeOfTheScreen(): Boolean {
    return this.left == 0
}


fun View.isOnTheRightEdgeOfTheScreen(): Boolean {
    return this.right == 0
}


fun View.index(parent: ViewGroup): Int {
    return parent.indexOfChild(this)
}


fun View.index(): Int {
    return (this.parent as ViewGroup).indexOfChild(this)
}


/**
 * returns all the views visible items from the view group [layout_random_words]
 */
fun ViewGroup.getVisibleItems(): List<View> {
    val visibleItems = arrayListOf<View>()
    this.forEach { view ->
        if (view.visibility == View.VISIBLE) {
            visibleItems.add(view)
        }
    }
    return visibleItems
}


/**
 * returns the views except the [indexes] from the view group [layout_random_words]
 */
fun ViewGroup.getRemainingItems(vararg indexes: Int): List<View> {
    val visibleItems = arrayListOf<View>()
    this.forEachIndexed { index, view ->
        if (!indexes.contains(index) && view.visibility == View.VISIBLE) {
            visibleItems.add(view)
        }
    }
    return visibleItems
}


/**
 * returns the views except the [views] from the view group [layout_random_words]
 */
fun ViewGroup.getRemainingItems(views: List<View>): List<View> {
    val visibleItems = arrayListOf<View>()
    this.forEach { view ->
        if (!views.contains(view) && view.visibility == View.VISIBLE) {
            visibleItems.add(view)
        }
    }
    return visibleItems
}


/**
 * returns the views which are previously highlighted in this group [layout_random_words]
 */
fun ViewGroup.getHighLightedViews(): List<View> {
    val visibleItems = arrayListOf<View>()
    this.forEach { view ->
        if (view.visibility == View.VISIBLE && (view.tag as WordItemViewData).highLightStatus == Constants.HIGHLIGHTED) {
            visibleItems.add(view)
        }
    }
    return visibleItems
}

/**
 * finds the previous  [View] from the [layout_random_words] uses recursion
 */
fun ViewGroup.getBeforeOrNull(index: Int): WordItemViewData? {
    return if (index >= 0) {
        val data = this[index].tag as WordItemViewData
        if (data.word?.typeWordOrPunctuation != Constants.TYPE_PUNCTUATION) {
            data
        } else {
            getBeforeOrNull(index - 1)
        }
    } else {
        null
    }
}


/**
 * finds the next [View] with tag [WordItemViewData] the [layout_random_words] uses recursion
 */
fun ViewGroup.getNextOrNull(index: Int): WordItemViewData? {
    return if (index < this.size) {
        val data = this[index].tag as WordItemViewData
        if (data.word?.typeWordOrPunctuation != Constants.TYPE_PUNCTUATION) {
            data
        } else {
            getNextOrNull(index + 1)
        }
    } else {
        null
    }
}


/**
 * finds the previous [View] with tag [WordItemViewData] the [layout_random_words] uses recursion
 */
fun ViewGroup.getPreviousViewOrNull(index: Int): View? {
    return if (index >= 0) {
        val data = this[index].tag as WordItemViewData
        if (data.word?.typeWordOrPunctuation != Constants.TYPE_PUNCTUATION) {
            this[index]
        } else {
            getPreviousViewOrNull(index - 1)
        }
    } else {
        null
    }
}

/**
 * finds the next [View] from the [layout_random_words] uses recursion
 */
fun ViewGroup.getNextViewOrNull(index: Int): View? {
    return if (index < this.size) {
        val data = this[index].tag as WordItemViewData
        if (data.word?.typeWordOrPunctuation != Constants.TYPE_PUNCTUATION) {
            this[index]
        } else {
            getNextViewOrNull(index + 1)
        }
    } else {
        null
    }
}
