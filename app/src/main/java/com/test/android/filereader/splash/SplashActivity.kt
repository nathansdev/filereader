package com.test.android.filereader.splash

import android.content.Intent
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.test.android.filereader.R
import com.test.android.filereader.base.BaseActivity
import com.test.android.filereader.main.FileReaderRecyclerActivity
import kotlinx.android.synthetic.main.activity_splash.*

/**
 * The launcher screen of application
 */
class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val zoomInAnimation: Animation = AnimationUtils.loadAnimation(
            this,
            R.anim.zoom_in
        )
        text_splash_screen.startAnimation(zoomInAnimation)
        zoomInAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                routeTo()
            }

            override fun onAnimationStart(p0: Animation?) {
            }

        })
    }

    /**
     * routes to main screen after the animation
     */
    private fun routeTo() {
        startActivity(Intent(this, FileReaderRecyclerActivity::class.java))
        finish()
    }
}
