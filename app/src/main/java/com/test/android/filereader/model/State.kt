package com.test.android.filereader.model

/**
 * FileReader activity's ui state model
 */
data class ViewState(
    val isLoading: Boolean = false,
    val randomLine: String = ""
)

/**
 * error state model
 */
data class ErrorState(
    var showError: Boolean = false,
    var message: String = ""
)