package com.test.android.filereader.utils

import android.graphics.Canvas
import android.graphics.Point
import android.graphics.drawable.Drawable
import android.view.View

/**
 * custom shadow class for dragged view with highlight background
 */
class CustomDragShadow(view: View, private val drawable: Drawable) : View.DragShadowBuilder(view) {
    private val draggedView = view

    override fun onProvideShadowMetrics(outShadowSize: Point?, outShadowTouchPoint: Point?) {
        // Define local variables
        val width: Int = draggedView.width
        val height: Int = draggedView.height

        // Sets the width of the shadow to half the width of the original view

        // Sets the height of the shadow to half the height of the original view

        // The drag shadow will fill the Canvas
        drawable.setBounds(0, 0, width, height)

        // Sets the size parameter's width and height values
        outShadowSize?.set(width, height)

        // Sets the touch point position to be in the middle of the drag shadow
        outShadowTouchPoint?.set(width / 2, height / 2)
    }

    override fun onDrawShadow(canvas: Canvas?) {
        if (canvas != null) {
            draggedView.setBackgroundDrawable(drawable)
            draggedView.draw(canvas)
        }
    }
}