package com.test.android.filereader.utils

import android.view.DragEvent
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.test.android.filereader.view.FlowLayout

/**
 * interface routes all drag and drop events to activity
 */
interface DragEventHandler {

    fun onEntered(view: TextView)

    fun onEnded(view: TextView)

    fun onLocation(view: RecyclerView, event: DragEvent)

    fun onDropped(view: RecyclerView, event: DragEvent)

    fun onEnded(view: RecyclerView, event: DragEvent)

    fun onEnded(view: FlowLayout, event: DragEvent)

    fun onExited(view: TextView)

    fun onDropped(view: FlowLayout, event: DragEvent)

    fun onDropped(view: TextView, event: DragEvent)

    fun onLocation(view: TextView, event: DragEvent)

    fun onLocation(view: FlowLayout, event: DragEvent)

    fun onDragStart(text: TextView);
}