package com.test.android.filereader.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.test.android.filereader.Constants
import com.test.android.filereader.R
import com.test.android.filereader.model.WordItemViewData

/**
 * Single Item Text view class for displaying words
 */
class WordItemView : TextView {
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr)

    companion object {

        /**
         * Returns single flow layout item.
         *
         * @param viewGroup item's view group.
         * @param text      text should be set.
         * @return layout item view.
         */
        fun createWordItemView(viewGroup: ViewGroup, tag: WordItemViewData): WordItemView {
            @LayoutRes val layoutId = R.layout.layout_word_item
            val itemView = LayoutInflater.from(viewGroup.context)
                .inflate(layoutId, viewGroup, false) as WordItemView
            itemView.setBackgroundResource(
                if (tag.word?.typeWordOrPunctuation == Constants.TYPE_WORD)
                    R.drawable.bg_word_green else R.drawable.bg_word_grey
            )
            itemView.text = tag.word?.value
            itemView.tag = tag
            return itemView
        }
    }
}