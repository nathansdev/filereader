package com.test.android.filereader.main

import android.content.ClipData
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.DragEvent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.size
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.test.android.filereader.Constants
import com.test.android.filereader.R
import com.test.android.filereader.base.BaseActivity
import com.test.android.filereader.extension.*
import com.test.android.filereader.model.ErrorState
import com.test.android.filereader.model.ViewState
import com.test.android.filereader.model.Word
import com.test.android.filereader.model.WordItemViewData
import com.test.android.filereader.utils.CustomDragShadow
import com.test.android.filereader.utils.DragEventHandler
import com.test.android.filereader.utils.Utils
import com.test.android.filereader.utils.WordDragListener
import com.test.android.filereader.view.FlowLayout
import com.test.android.filereader.view.WordItemView
import kotlinx.android.synthetic.main.activity_words_flow_layout.*
import timber.log.Timber
import java.util.regex.Pattern


/**
 * unused
 * The main screen of application shows list of words using flowlayout
 */
class FileReaderFlowLayoutActivity : BaseActivity(), View.OnClickListener,
    View.OnLongClickListener, DragEventHandler, ViewGroup.OnHierarchyChangeListener {

    private lateinit var viewModel: FileReaderViewModel
    private var listOfWords: ArrayList<Word> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_words_flow_layout)
        setUpViews()
        setUpViewModel()
    }

    /**
     * initialize views
     */
    private fun setUpViews() {
        button_next.setOnClickListener(this)
        if (layout_random_words != null) {
            layout_random_words.removeAllViews()
        }
        layout_random_words.setOnDragListener(WordDragListener(this))
        layout_random_words.setOnHierarchyChangeListener(this)
    }

    /**
     * initialize view model and observes for state changes
     */
    private fun setUpViewModel() {
        viewModel = this.let {
            ViewModelProvider(this)[FileReaderViewModel::class.java]
        }
        viewModel.viewState.observe(this, Observer {
            it?.let { renderUI(it) }
        })
        viewModel.errorState.observe(this, Observer {
            it?.let { renderError(it) }
        })
        viewModel.loadFileFromAsset()
    }

    /**
     * update the ui when state changes
     */
    private fun renderUI(state: ViewState) {
        when (state.isLoading) {
            true -> {
                progress_main.show()
                button_next.gone()
            }
            false -> {
                progress_main.gone()
                button_next.show()
            }
        }
        setUpWords(state.randomLine)
    }

    /**
     * renders error for this activity
     */
    private fun renderError(state: ErrorState) {
        when (state.showError) {
            true -> {
                showToast(state.message)
                button_next.gone()
            }
        }
    }

    /**
     * adds words from randomLine to [layout_random_words]
     */
    private fun setUpWords(text: String) {
        if (text.isEmpty()) {
            return
        }
        if (layout_random_words != null) {
            layout_random_words.removeAllViews()
        }
        if (listOfWords.isNotEmpty()) {
            listOfWords.clear()
        }

        val m =
            Pattern.compile(Constants.REGEX_WORDS_SEPARATION_BY_SPACE_AND_PUNCTUATION).matcher(text)
        while (m.find()) {
            val word = Word(
                value = m.group(),
                typeWordOrPunctuation = if (m.group().hasPunctuations()) Constants.TYPE_PUNCTUATION else Constants.TYPE_WORD
            )
            listOfWords.add(word)
        }
        Timber.d(
            "list of words separated by regex %s %s",
            listOfWords,
            listOfWords.size
        )
        listOfWords.forEachIndexed { index, it ->
                val wordItemViewData =
                    WordItemViewData(
                        word = it,
                        prefix = listOfWords.getBeforeOrNull(index - 1),
                        suffix = listOfWords.getNextOrNull(index + 1)
                    )
                val wordItemView = WordItemView.createWordItemView(
                    layout_random_words, wordItemViewData
                )
                wordItemView.setOnClickListener(this)
                wordItemView.setOnLongClickListener(this)
                wordItemView.setOnDragListener(WordDragListener(this))
                layout_random_words.addView(wordItemView)
        }
        if (layout_random_words != null) {
            layout_random_words.requestLayout()
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.button_next -> {
                viewModel.getRandomLineFromList()
            }
        }
        if (view?.tag != null) {
            val viewData = view.tag as WordItemViewData
            when (viewData.word?.typeWordOrPunctuation) {
                Constants.TYPE_WORD -> {
                    showToast(Constants.TOAST_PREFIX_TEXT + viewData.word.value)
                }
            }
        }
    }

    override fun onLongClick(view: View?): Boolean {
        val viewData = view?.tag as WordItemViewData
        when (viewData.word?.typeWordOrPunctuation) {
            Constants.TYPE_WORD -> dragLongPressedWord(view as TextView)
        }
        return true
    }

    override fun onEnded(view: TextView) {
        changeWordBackgrounds(arrayListOf(view))
    }

    override fun onExited(view: TextView) {
        changeWordBackgrounds(layout_random_words.getHighLightedViews())
    }

    override fun onEntered(view: TextView) {

    }

    /**
     * handles the on drag call on views when the another view hover on them
     */
    override fun onLocation(view: TextView, event: DragEvent) {
        val viewData = view.tag as WordItemViewData
        when (viewData.word?.typeWordOrPunctuation) {
            Constants.TYPE_PUNCTUATION -> {
                when (Utils.isDragPointInsideLeftOrRightHalfOfView(view, event)) {
                    Constants.INDEX_LEFT -> {
                        val views = view.getCurrentAndPreviousView(layout_random_words)
                        if (views.size == 2) {
                            changeWordBackgrounds(
                                layout_random_words.getRemainingItems(views)
                            )
                            highLightTargetView(views)
                        } else {
                            changeWordBackgrounds(layout_random_words.getVisibleItems())
                        }
                    }
                    Constants.INDEX_RIGHT -> {
                        val views = view.getCurrentAndNextView(layout_random_words)
                        Timber.d("views %s", views.size)
                        if (views.size == 2) {
                            changeWordBackgrounds(
                                layout_random_words.getRemainingItems(views)
                            )
                            highLightTargetView(views)
                        } else {
                            changeWordBackgrounds(layout_random_words.getVisibleItems())
                        }
                    }
                }
            }
            Constants.TYPE_WORD -> {
                when (val indexOfThisView = view.index(layout_random_words)) {
                    0 -> {
                        changeWordBackgrounds(
                            layout_random_words.getRemainingItems(
                                indexOfThisView
                            )
                        )
                        highLightTargetView(arrayListOf(view))
                    }
                    layout_random_words.size - 1 -> {
                        changeWordBackgrounds(
                            layout_random_words.getRemainingItems(
                                indexOfThisView
                            )
                        )
                        highLightTargetView(arrayListOf(view))
                    }
                    else -> {
                        when (Utils.isDragPointInsideLeftOrRightHalfOfView(view, event)) {
                            Constants.INDEX_LEFT -> {
                                val views = view.getCurrentAndPreviousView(layout_random_words)
                                if (views.size == 2) {
                                    changeWordBackgrounds(
                                        layout_random_words.getRemainingItems(views)
                                    )
                                    highLightTargetView(views)
                                } else {
                                    changeWordBackgrounds(layout_random_words.getVisibleItems())
                                }
                            }
                            Constants.INDEX_RIGHT -> {
                                val views = view.getCurrentAndNextView(layout_random_words)
                                Timber.d("views %s", views.size)
                                if (views.size == 2) {
                                    changeWordBackgrounds(
                                        layout_random_words.getRemainingItems(views)
                                    )
                                    highLightTargetView(views)
                                } else {
                                    changeWordBackgrounds(layout_random_words.getVisibleItems())
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onLocation(view: FlowLayout, event: DragEvent) {

    }

    override fun onDragStart(text: TextView) {

    }

    override fun onDropped(view: TextView, event: DragEvent) {
        val viewData = view.tag as WordItemViewData
        val droppedView = event.localState as View as TextView
        val indexOfDroppedView = droppedView.index(layout_random_words)
        Timber.d(
            "dropped text, drag point text, dropped view index, drag point location %s %s %s %s",
            droppedView.text,
            view.text,
            indexOfDroppedView,
            Utils.isDragPointInsideLeftOrRightHalfOfView(view, event)
        )
        when (viewData.word?.typeWordOrPunctuation) {
            Constants.TYPE_PUNCTUATION -> {
                when (val indexOfThisView = view.index(layout_random_words)) {
                    0 -> {
                        removeViewAt(indexOfDroppedView)
                        insertViewAt(event.localState as View, indexOfThisView + 1)
                    }
                    layout_random_words.size - 1 -> {
                        removeViewAt(indexOfDroppedView)
                        insertViewAt(event.localState as View, indexOfThisView - 1)
                    }
                    else -> {
                        when (Utils.isDragPointInsideLeftOrRightHalfOfView(view, event)) {
                            Constants.INDEX_RIGHT -> {
                                removeViewAt(indexOfDroppedView)
                                insertViewAt(
                                    event.localState as View,
                                    view.index(layout_random_words) + 1
                                )
                            }
                            Constants.INDEX_LEFT -> {
                                removeViewAt(indexOfDroppedView)
                                insertViewAt(
                                    event.localState as View,
                                    view.index(layout_random_words)
                                )
                            }
                        }
                    }
                }
            }
            Constants.TYPE_WORD -> {
                when (val indexOfThisView = view.index(layout_random_words)) {
                    0 -> {
                        removeViewAt(indexOfDroppedView)
                        insertViewAt(event.localState as View, indexOfThisView)
                    }
                    layout_random_words.size - 1 -> {
                        removeViewAt(indexOfDroppedView)
                        insertViewAt(event.localState as View, indexOfThisView)
                    }
                    else -> {
                        when (Utils.isDragPointInsideLeftOrRightHalfOfView(view, event)) {
                            Constants.INDEX_RIGHT -> {
                                removeViewAt(indexOfDroppedView)
                                insertViewAt(
                                    event.localState as View,
                                    view.index(layout_random_words) + 1
                                )
                            }
                            Constants.INDEX_LEFT -> {
                                removeViewAt(indexOfDroppedView)
                                insertViewAt(
                                    event.localState as View,
                                    view.index(layout_random_words)
                                )
                            }
                        }
                    }
                }
            }
        }
        changeWordBackgrounds(arrayListOf(view))
    }

    override fun onDropped(view: FlowLayout, event: DragEvent) {
        val viewDragged = event.localState as View
        viewDragged.show()
    }

    override fun onEnded(view: FlowLayout, event: DragEvent) {
        val viewDraggedAndEnded = event.localState as View
        viewDraggedAndEnded.show()
        changeWordBackgrounds(arrayListOf(viewDraggedAndEnded))
    }

    override fun onChildViewRemoved(parent: View?, child: View?) {

    }

    override fun onChildViewAdded(parent: View?, child: View?) {
        Timber.d("onChildViewAdded ")
    }

    override fun onDropped(view: RecyclerView, event: DragEvent) {
    }

    override fun onLocation(view: RecyclerView, event: DragEvent) {
    }

    override fun onEnded(view: RecyclerView, event: DragEvent) {
    }

    /**
     * starts dragging the long pressed view on the screen
     */
    private fun dragLongPressedWord(view: TextView?) {
        val data = ClipData.newPlainText("value", view?.text)
        view?.startDrag(
            data,
            CustomDragShadow(
                view,
                ColorDrawable(R.drawable.bg_word_high_light)
            ),
            view, 0
        )
        view?.hide()
    }

    /**
     *insert [view] at in between [index] of view group [layout_random_words]]
     */
    private fun insertViewAt(view: View, index: Int) {
        Timber.d("insert view at %s %s", view, index)
        layout_random_words.addView(view, index)
    }

    /**
     *removes view at [index] from view group [layout_random_words]]
     */
    private fun removeViewAt(index: Int) {
        Timber.d("remove view at %s", index)
        layout_random_words.removeViewAt(index)
    }

    /**
     * changes the views background based on drag and drop events
     */
    private fun changeWordBackgrounds(views: List<View>) {
        views.forEach {
            val highLightView = it as TextView
            highLightView.apply {
                setBackgroundResource(getBackgroundResource(it))
                tag = (tag as WordItemViewData).copy(highLightStatus = Constants.NORMAL)
                invalidate()
            }
        }
    }

    /**
     * returns the background based on prefix and suffix check
     */
    private fun getBackgroundResource(view: View): Int {
        val currentViewData = view.tag as WordItemViewData
        when (currentViewData.word?.typeWordOrPunctuation) {
            Constants.TYPE_PUNCTUATION -> {
                return R.drawable.bg_word_grey
            }
            Constants.TYPE_WORD -> {
                when (val index = view.index(layout_random_words)) {
                    0 -> {
                        val nextViewData = layout_random_words.getNextOrNull(index + 1)
                        return if (currentViewData.suffix.equals(nextViewData?.word?.value)) {
                            R.drawable.bg_word_green
                        } else {
                            R.drawable.bg_word_red
                        }
                    }
                    layout_random_words.size - 1 -> {
                        val previousViewData = layout_random_words.getBeforeOrNull(index - 1)
                        return if (currentViewData.prefix.equals(previousViewData?.word?.value)) {
                            R.drawable.bg_word_green
                        } else {
                            R.drawable.bg_word_red
                        }
                    }
                    else -> {
                        val previousViewData = layout_random_words.getBeforeOrNull(index - 1)
                        val nextViewData = layout_random_words.getNextOrNull(index + 1)
                        return if (currentViewData.prefix.equals(previousViewData?.word?.value)
                            && currentViewData.suffix.equals(nextViewData?.word?.value)
                        ) {
                            R.drawable.bg_word_green
                        } else if (!currentViewData.prefix.equals(previousViewData?.word?.value)
                            && !currentViewData.suffix.equals(nextViewData?.word?.value)
                        ) {
                            R.drawable.bg_word_red
                        } else {
                            R.drawable.bg_word_yellow
                        }
                    }
                }
            }
            else -> {
                return R.drawable.bg_word_grey
            }
        }
    }

    /**
     * highlight the views background to blue on hover
     */
    private fun highLightTargetView(views: List<View>) {
        views.forEach {
            val highLightView = it as TextView
            highLightView.apply {
                setBackgroundResource(R.drawable.bg_word_high_light)
                tag = (tag as WordItemViewData).copy(highLightStatus = Constants.HIGHLIGHTED)
                invalidate()
            }
        }
    }
}
