package com.test.android.filereader.utils

import ShiftableArrayList
import android.graphics.Point
import android.graphics.Rect
import android.view.DragEvent
import android.view.View
import androidx.core.view.marginLeft
import androidx.core.view.marginRight
import androidx.recyclerview.widget.RecyclerView
import com.test.android.filereader.Constants
import com.test.android.filereader.extension.getLocationOnScreen
import com.test.android.filereader.model.WordItemViewData
import timber.log.Timber
import kotlin.math.roundToInt


object Utils {

    /**
     * unused method
     * checks whether the view under x and y location
     */
    fun isViewUnderInScreen(view: View?, x: Int, y: Int): Boolean {
        val mTempLocation = IntArray(2)
        if (view == null) {
            return false
        }
        val width = view.width
        val height = view.height
        view.getLocationOnScreen(mTempLocation)
        val viewX = mTempLocation[0]
        val viewY = mTempLocation[1]
        Timber.d("Inside view %s %s %s %s", width, height, viewX, viewY)
        return (x >= viewX && x < viewX + width
                && y >= viewY && y < viewY + height)
    }

    /**
     * Determines if given points are inside view
     * @param x - x coordinate of point
     * @param y - y coordinate of point
     * @param view - view object to compare
     * @return true if the points are within view bounds, false otherwise
     */
    fun isPointInsideViewBounds(x: Float, y: Float, view: View): Boolean {
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        val viewX = location[0]
        val viewY = location[1]

        // point is inside view bounds
        return x >= viewX && x < viewX + view.width && y >= viewY && y < viewY + view.height
    }

    /**
     * Check whether the drag event point is on the left or right side of the underlying view
     * @return index 0 for left side and 1 for right side
     */
    fun isDragPointInsideLeftOrRightHalfOfView(view: View, dragEvent: DragEvent): Int {
        val width = view.width
        val height = view.height
        val absXY = view.getLocationOnScreen()
        val centerX = absXY.x + width / 2
        val centerY = absXY.y + height / 2
        Timber.d("view x, y, l, t  %s %s %s %s", centerX, centerY, view.left, view.top)
        val absTouch = getTouchPositionFromDragEvent(view, dragEvent)
        val touchX = absTouch.x
        val touchY = absTouch.y
        Timber.d("touch  x, y %s %s", touchX, touchY)
        if (touchX >= centerX) {
            return Constants.INDEX_RIGHT
        }
        return Constants.INDEX_LEFT
    }

    /**
     * @return the absolute x and y point of dragged event in screen
     */
    private fun getTouchPositionFromDragEvent(item: View, event: DragEvent): Point {
        val rItem = Rect()
        item.getGlobalVisibleRect(rItem)
        return Point(rItem.left + event.x.roundToInt(), rItem.top + event.y.roundToInt())
    }

    /**
     * unused method
     *
     */
    fun getUpdatedListOfWords(
        listOfOldItems: ArrayList<WordItemViewData>,
        indexesOfPunctuations: ArrayList<Int>,
        indexOfNewItem: Int,
        newItem: WordItemViewData
    ): ArrayList<WordItemViewData> {
        Timber.d(
            "getUpdatedListOfWords %s %s %s %s",
            listOfOldItems,
            indexesOfPunctuations,
            indexOfNewItem,
            newItem
        )
        val listOfNewItems = arrayListOf<WordItemViewData>()
        var count = 0
        listOfOldItems.forEachIndexed { index, wordItemViewData ->
            if (index <= newItem.position!!) {
                if (indexesOfPunctuations.contains(index)) {
                    listOfNewItems.add(wordItemViewData)
                    if (count != 0) {
                        count++
                    }
                } else {
                    if (index == indexOfNewItem) {
                        listOfNewItems.add(newItem)
                        count++
                    } else {
                        listOfNewItems.add(listOfOldItems[index - count])
                    }
                }
            } else {
                listOfNewItems.add(wordItemViewData)
            }
            if (listOfNewItems[index].isWord) {
                count--
            }
        }
        return listOfNewItems
    }

    /**
     * Removes and moves dragged element to new index and reorders fixed indexes
     */
    fun getReOrderedListOfWords(
        listOfOldItems: ArrayList<WordItemViewData>,
        indexesOfPunctuations: ArrayList<Int>,
        indexOfNewItem: Int,
        newItem: WordItemViewData
    ): ArrayList<WordItemViewData> {
        Timber.d(
            "getReOrderedListOfWords %s %s %s %s",
            listOfOldItems.size,
            indexesOfPunctuations,
            indexOfNewItem,
            newItem
        )
        val listOfNewItems = ArrayList<WordItemViewData>()
        val shiftedArrayList = ShiftableArrayList<WordItemViewData>()
        shiftedArrayList.addAll(listOfOldItems)
        //updating dragged item visibility
        shiftedArrayList[newItem.position!!] = newItem.copy(show = true)
        // setting fixed indexes for punctuations
        shiftedArrayList.setFixedPositions(indexesOfPunctuations)
        shiftedArrayList.shift(newItem.position, indexOfNewItem)

        //updating the index positions
        shiftedArrayList.forEachIndexed { index, wordItemViewData ->
            listOfNewItems.add(index, wordItemViewData.copy(position = index))
        }
        return listOfNewItems
    }

    /**
     * finds left and right view holder for drag event x, y points
     */
    fun findTwoViewHolderUnderTouchPointBounds(
        recyclerView: RecyclerView,
        event: DragEvent
    ): List<RecyclerView.ViewHolder> {
        val holders = arrayListOf<RecyclerView.ViewHolder>()
        val childCount = recyclerView.adapter?.itemCount
        val absTouch = getTouchPositionFromDragEvent(recyclerView, event)
        Timber.d("absolute touch point %s %s", absTouch.x, absTouch.y)
        for (i in 0 until childCount!!) {
            val child = recyclerView.getChildAt(i)
            val tag = (child.tag as WordItemViewData)
            Timber.d("tag %s", tag)
            if (tag.isWord && tag.show) {
                if (isTouchPointInsideRightHalfOfView(child, absTouch.x, absTouch.y)) {
                    holders.add(recyclerView.getChildViewHolder(child))
                }
                if (isTouchPointInsideLeftHalfOfView(child, absTouch.x, absTouch.y)) {
                    holders.add(recyclerView.getChildViewHolder(child))
                }
            }
        }
        return holders
    }


    /**
     * finds the closest view data and its drag position side
     */
    fun getViewDataAndDragPositionUnderTouchPoint(
        recyclerView: RecyclerView,
        event: DragEvent
    ): Pair<Int, WordItemViewData>? {
        val childCount = recyclerView.adapter?.itemCount
        val absTouch = getTouchPositionFromDragEvent(recyclerView, event)
        Timber.d("absolute touch point %s %s", absTouch.x, absTouch.y)
        for (i in 0 until childCount!!) {
            val child = recyclerView.getChildAt(i)
            val tag = (child.tag as WordItemViewData)
            Timber.d("tag %s", tag)
            if (tag.show) {
                if (isTouchPointInsideRightHalfOfView(child, absTouch.x, absTouch.y)) {
                    return Pair(Constants.INDEX_RIGHT, tag)
                }
                if (isTouchPointInsideLeftHalfOfView(child, absTouch.x, absTouch.y)) {
                    return Pair(Constants.INDEX_LEFT, tag)
                }
            }
        }
        return null
    }


    /**
     * check the drag point is still between view edge to its right margin
     */
    private fun isTouchPointInsideRightHalfOfView(view: View, x: Int, y: Int): Boolean {
        val absXY = view.getLocationOnScreen()
        val viewX = absXY.x + view.width / 2
        val viewY = absXY.y
        Timber.d(
            "right side of view x, y, w, h, m %s %s %s %s %s",
            viewX,
            viewY,
            view.width,
            view.height,
            view.marginRight
        )
        // point is inside view bounds
        return x >= viewX && x < viewX + view.width / 2 + view.marginRight && y >= viewY && y < viewY + view.height
    }

    /**
     * check the drag point is still between view edge to its left margin
     */
    private fun isTouchPointInsideLeftHalfOfView(view: View, x: Int, y: Int): Boolean {
        val absXY = view.getLocationOnScreen()
        val viewX = absXY.x + view.width / 2
        val viewY = absXY.y
        Timber.d(
            "left side of view x, y, w, h, m %s %s %s %s %s",
            viewX,
            viewY,
            view.width,
            view.height,
            view.marginLeft
        )
        // point is inside view bounds
        return x >= viewX - view.width / 2 - view.marginLeft && x < viewX && y >= viewY && y < viewY + view.height
    }
}