package com.test.android.filereader

/**
 * The Entire app constants goes here
 */
object Constants {

    const val FILE_NAME = "Text_Entries.txt"
    const val TOAST_PREFIX_TEXT = "You clicked on the word : "

    const val HIGHLIGHTED = "highlighted"
    const val NORMAL = "normal"

    const val PUNCTUATION_REGEX = "[\\p{Punct}&&[^']&&[^-]]|(?<![a-zA-Z])'|'(?![a-zA-Z])"

    const val REGEX_WORDS_SEPARATION_BY_SPACE_AND_PUNCTUATION =
        "[\\p{L}\\p{M}\\p{N}]+(?:\\p{P}[\\p{L}\\p{M}\\p{N}]+)*|[\\p{P}\\p{S}]"

    const val INDEX_LEFT = 0
    const val INDEX_RIGHT = 1

    const val TYPE_PUNCTUATION = 0
    const val TYPE_WORD = 1
}
