package com.test.android.filereader.main.adapter

import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.test.android.filereader.Constants
import com.test.android.filereader.model.WordItemViewData
import timber.log.Timber
import java.util.*

/**
 * Unused
 * DataSet class for ordering which implements [SortedListAdapterCallback]
 */
class WordsAdapterDataSet constructor(
    adapter: WordsAdapter,
    data: ArrayList<WordItemViewData>?
) : SortedListAdapterCallback<WordItemViewData>(adapter) {
    override fun areItemsTheSame(item1: WordItemViewData, item2: WordItemViewData): Boolean {
        return areItemsSame(item1, item2)
    }

    override fun compare(o1: WordItemViewData, o2: WordItemViewData): Int {
        return compareTo(o1, o2)
    }

    override fun areContentsTheSame(
        oldItem: WordItemViewData,
        newItem: WordItemViewData
    ): Boolean {
        return areContentsSame(oldItem, newItem)
    }

    private val sortedList: SortedList<WordItemViewData> =
        SortedList<WordItemViewData>(WordItemViewData::class.java, this)

    private fun areItemsSame(
        item2: WordItemViewData,
        item21: WordItemViewData
    ): Boolean {
        if (item2.isPunctuation && item21.isPunctuation) {
            return item2.position == item21.position
        }
        return false
    }

    private fun compareTo(o1: WordItemViewData, o2: WordItemViewData): Int {
        Timber.d("compareTo %s %s", o1, o2)
        if (o1.word?.typeWordOrPunctuation == o2.word?.typeWordOrPunctuation) {
            when {
                o1.position == o2.position -> return 0
                o1.position!! > o2.position!! -> return 1
                o1.position < o2.position -> return -1
            }
        } else if (o1.isWord && o2.isPunctuation) {
            when {
                o1.position!! > o2.position!! -> return 1
                o1.position < o2.position -> return -1
            }
        } else if (o1.isPunctuation && o2.isWord) {
            when {
                o1.position!! > o2.position!! -> return 1
                o1.position < o2.position -> return -1
            }
        }
        return o1.position!!.compareTo(o2.position!!)
    }

    private fun areContentsSame(
        item2: WordItemViewData,
        item21: WordItemViewData
    ): Boolean {
        if (item2.isPunctuation && item21.isPunctuation) {
            return item2.position == item21.position
        }
        return false
    }

    val isEmpty: Boolean
        get() = sortedList.size() == 0

    /**
     * @return ArrayList representation of the sorted list.
     */
    val arrayList: ArrayList<WordItemViewData>
        get() {
            val list = ArrayList<WordItemViewData>()
            for (i in 0 until sortedList.size()) {
                list.add(sortedList.get(i))
            }
            return list
        }

    init {
        if (data != null && data.isNotEmpty()) {
            sortedList.addAll(data)
        }
    }

    fun handleDestroy() {
        sortedList.clear()
    }

    fun size(): Int {
        return sortedList.size()
    }

    fun add(list: List<WordItemViewData>) {
        list.forEach {
            sortedList.add(it)
        }
    }

    fun add(item: WordItemViewData, index: Int) {
        sortedList.add(item)
    }

    fun update(item: WordItemViewData, position: Int) {
        sortedList.updateItemAt(position, item)
    }

    fun updateHighLightedListToNormal() {
        if (highlightedList.isNotEmpty()) {
            Timber.d("highlightedList size %s", highlightedList.size)
            highlightedList.forEach {
                val newItem = it.copy(highLightStatus = Constants.NORMAL)
                update(newItem, newItem.position!!)
            }
        }
    }

    fun remove(index: Int) {
        sortedList.removeItemAt(index)
    }

    /**
     * @return ArrayList representation of the highlighted list.
     */
    private val highlightedList: ArrayList<WordItemViewData>
        get() {
            val list = ArrayList<WordItemViewData>()
            for (i in 0 until sortedList.size()) {
                if (sortedList.get(i).highLightStatus == Constants.HIGHLIGHTED) {
                    list.add(sortedList.get(i))
                }
            }
            return list
        }

    operator fun get(position: Int): WordItemViewData {
        return sortedList.get(position)
    }
}
