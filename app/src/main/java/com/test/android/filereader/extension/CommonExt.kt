package com.test.android.filereader.extension

import android.content.res.Resources
import androidx.recyclerview.widget.RecyclerView
import com.test.android.filereader.Constants
import com.test.android.filereader.main.adapter.WordsAdapter
import com.test.android.filereader.model.Word
import com.test.android.filereader.model.WordItemViewData
import java.util.regex.Pattern


/**
 * common extension functions
 */
fun Any.name(i: Int): String {
    return this.toString().split(" ")[i]
}


/**
 * matches given word with all the punctuations chars
 * except apostrophe in the middle
 */
fun String.hasPunctuations(): Boolean {
    val p = Pattern.compile(Constants.PUNCTUATION_REGEX)
    val m = p.matcher(this)
    return m.find()
}


fun Float.dpToPx(resources: Resources) = this * resources.displayMetrics.density


fun Int.dpToPx(resources: Resources) = (this * resources.displayMetrics.density).toInt()


/**
 * finds the previous [Word] from the list
 */
fun ArrayList<Word>.getBeforeOrNull(index: Int): String? {
    return if (index >= 0) {
        if (this[index].typeWordOrPunctuation != Constants.TYPE_PUNCTUATION) {
            this[index].value
        } else {
            getBeforeOrNull(index - 1)
        }
    } else {
        null
    }
}


/**
 * finds the next [Word] from the list
 */
fun ArrayList<Word>.getNextOrNull(index: Int): String? {
    return if (index < this.size) {
        if (this[index].typeWordOrPunctuation != Constants.TYPE_PUNCTUATION) {
            this[index].value
        } else {
            getNextOrNull(index + 1)
        }
    } else {
        null
    }
}


/**
 * Extension method for notifying recycler view adapter
 */
fun RecyclerView.Adapter<WordsAdapter.WordItemViewHolder>.notify() {
    this.notifyDataSetChanged()
}


/**
 * returns all the highlighted items from the list
 */
fun ArrayList<WordItemViewData>.getHighLightedItems(): ArrayList<WordItemViewData> {
    val list = arrayListOf<WordItemViewData>()
    this.forEach {
        if (it.highLightStatus == Constants.HIGHLIGHTED) {
            list.add(it)
        }
    }
    return list
}


/**
 * checks if  current and previous [WordItemViewData] is visible and returns both
 */
fun ArrayList<WordItemViewData>.getThisAndPreviousAdapterWordData(index: Int): ArrayList<WordItemViewData> {
    val list = arrayListOf<WordItemViewData>()
    if (index >= 0) {
        val first = this[index]
        val second = this.getPreviousAdapterWordData(index - 1)
        if (second != null) {
            if (second.show && second.isWord && first.show && first.isWord) {
                list.add(first)
                list.add(second)
            }
        } else {
            if (first.show) {
                list.add(first)
            }
        }
    }
    return list
}


/**
 * checks if  current and next [WordItemViewData] is visible and returns both
 */
fun ArrayList<WordItemViewData>.getThisAndNextAdapterWordData(index: Int): ArrayList<WordItemViewData> {
    val list = arrayListOf<WordItemViewData>()
    if (index < this.size) {
        val first = this[index]
        val second = this.getNextAdapterWordData(index + 1)
        if (second != null) {
            if (second.show && second.isWord && first.show && first.isWord) {
                list.add(first)
                list.add(second)
            }
        } else {
            if (first.show) {
                list.add(first)
            }
        }
    }
    return list
}


/**
 * finds the previous [WordItemViewData] to the current index from the list uses recursion
 */
fun ArrayList<WordItemViewData>.getPreviousAdapterWordData(index: Int): WordItemViewData? {
    return if (index >= 0) {
        val data = this[index]
        if (data.isWord) {
            data
        } else {
            getPreviousAdapterWordData(index - 1)
        }
    } else {
        null
    }
}


/**
 * finds the next [WordItemViewData] to the current index from the list uses recursion
 */
fun ArrayList<WordItemViewData>.getNextAdapterWordData(index: Int): WordItemViewData? {
    return if (index < this.size) {
        val data = this[index]
        if (data.isWord) {
            data
        } else {
            getNextAdapterWordData(index + 1)
        }
    } else {
        null
    }
}


/**
 * finds the next and current [WordItemViewData] to the current index from the list uses recursion
 */
fun ArrayList<WordItemViewData>.getPreviousAndNextWordData(index: Int): ArrayList<WordItemViewData> {
    val list = arrayListOf<WordItemViewData>()
    val previousData = getPreviousAdapterWordData(index)
    val nextData = getNextAdapterWordData(index)

    //checks null for words before and after to punctuations in the middle of sentence
    if (previousData != null && previousData.show && nextData != null && nextData.show) {
        list.add(previousData)
        list.add(nextData)
        return list
    } else {
        //nextData will be null if punctuations at the last doesn't have words next to it
        if (previousData != null && nextData == null) {
            if (previousData.show) {
                list.add(previousData)
            }
            //previousData will be null if punctuations at the beginning doesn't have words before to it
        } else if (nextData != null && previousData == null) {
            if (nextData.show) {
                list.add(nextData)
            }
        }
    }
    return list
}


/**
 * finds the next [WordItemViewData] to the current index from the list uses recursion
 */
fun ArrayList<WordItemViewData>.isItemVisibleAt(index: Int): Boolean? {
    var isVisible = false
    val data = this[index]
    if (data.show) {
        isVisible = true
    }
    return isVisible
}