package com.test.android.filereader

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.test.android.filereader.main.FileReaderViewModel
import com.test.android.filereader.model.ViewState
import junit.framework.TestCase.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
 * Unit tests for the implementation of [FileReaderViewModel].
 */
@RunWith(JUnit4::class)
class FileReaderViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    var observer: Observer<ViewState>? = null

    @Mock
    var asyncTask: FileReaderViewModel.LoadFileFromAssetTask? = null

    private lateinit var viewModel: FileReaderViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = FileReaderViewModel(application = Application())
        viewModel.viewState.observeForever(observer!!)
        viewModel.loadTask = asyncTask
    }

    @After
    fun tearDown() {

    }

    @Test
    fun testNull() {
        assertNotNull(viewModel.viewState)
        assert(viewModel.viewState.hasObservers())
    }

    @Test
    fun testViewModelOnStateChange() {
        // Load the task in the viewmodel
        viewModel.loadFileFromAsset()

        // Load the task in the viewmodel
        viewModel.loadingStarted()

        // Then progress indicator is shown
        assertTrue(viewModel.currentViewState().isLoading)

        // stop the task in the viewmodel
        viewModel.loadingFinished()

        assertFalse(viewModel.currentViewState().isLoading)

        //test error state change with message
        val errorMessage = "Test Error Messaage"

        viewModel.onError(errorMessage)

        assertTrue(viewModel.currentErrorState().showError)

        assertEquals(viewModel.currentErrorState().message, errorMessage)

        //test random line change methods
        val randomLine = "Random Line"

        viewModel.randomLineChanged(randomLine)

        assertEquals(viewModel.currentViewState().randomLine, randomLine)
    }
}